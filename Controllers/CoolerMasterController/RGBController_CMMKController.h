/*-------------------------------------------------------------------*\
|  RGBController_CMMKController.h                                     |
|                                                                     |
|  Driver for Coolermaster MasterKeys keyboards                       |
|                                                                     |
|  Lukas N (chmod222)          28th Jun 2020                          |
|  Tam D (too.manyhobbies)     25th Apr 2021                          |
|                                                                     |
\*-------------------------------------------------------------------*/

#pragma once

#include "RGBController.h"
#include "CMMKController.h"

class RGBController_CMMKController : public RGBController
{
public:
    RGBController_CMMKController(CMMKController* controller_ptr);
    ~RGBController_CMMKController();

    void SetupZones();
    void ResizeZone(int zone, int new_size);

    void DeviceUpdateLEDs();
    void UpdateZoneLEDs(int zone);
    void UpdateSingleLED(int led);

    void SetCustomMode();
    void DeviceUpdateMode();

private:
    CMMKController* controller;

    std::string ledname_ansi[6][22] = {
        {std::string("Escape"), "F1", "F2", "F3", "F4", "XXX", "F5", "F6", "F7", "F8", "XXX", "F9", "F10", "F11", "F12", "Print Screen", "Scroll Lock", "Pause/Break", "P1", "P2", "P3", "P4"},
        {"`", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "-", "=", "XXX", "Backspace", "Insert", "Home", "Page Up", "#LCK", "Number Pad /", "Number Pad *", "Number Pad -"},
        {"Tab", "Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P", "[", "]", "XXX", "\\ (ANSI)", "Delete", "End", "Page Down", "Number Pad 7", "Number Pad 8", "Number Pad 9", "Number Pad +"},
        {"Caps Lock", "A", "S", "D", "F", "G", "H", "J", "K", "L", ";", "'", "XXX", "XXX", "Enter", "XXX", "XXX", "XXX", "Number Pad 4", "Number Pad 5", "Number Pad 6", "XXX"},
        {"Left Shift", "XXX", "Z", "X", "C", "V", "B", "N", "M", ",", ".", "/", "XXX", "XXX", "Right Shift", "XXX", "Up Arrow", "XXX", "Number Pad 1", "Number Pad 2", "Number Pad 3", "Number Pad Enter"},
        {"Left Control", "Left Windows", "Left Alt", "XXX", "XXX", "XXX", "Space", "XXX", "XXX", "XXX", "Right Alt", "Right Windows", "Right Fn", "XXX", "Right Control", "Left Arrow", "Down Arrow", "Right Arrow", "Number Pad 0", "XXX", "Number Pad .", "XXX"}
    };

    std::string ledname_iso[6][22] = {
        {std::string("Escape"), "F1", "F2", "F3", "F4", "XXX", "F5", "F6", "F7", "F8", "XXX", "F9", "F10", "F11", "F12", "Print Screen", "Scroll Lock", "Pause/Break", "P1", "P2", "P3", "P4"},
        {"`", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "-", "=", "XXX", "Backspace", "Insert", "Home", "Page Up", "#LCK", "Number Pad /", "Number Pad *", "Number Pad -"},
        {"Tab", "Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P", "[", "]", "XXX", "Enter (ISO)", "Delete", "End", "Page Down", "Number Pad 7", "Number Pad 8", "Number Pad 9", "Number Pad +"},
        {"Caps Lock", "A", "S", "D", "F", "G", "H", "J", "K", "L", ";", "'", "#", "XXX", "XXX", "XXX", "XXX", "XXX", "Number Pad 4", "Number Pad 5", "Number Pad 6", "XXX"},
        {"Left Shift", "\\ (ISO)", "Z", "X", "C", "V", "B", "N", "M", ",", ".", "/", "XXX", "XXX", "Right Shift", "XXX", "Up Arrow", "XXX", "Number Pad 1", "Number Pad 2", "Number Pad 3", "Number Pad Enter"},
        {"Left Control", "Left Windows", "Left Alt", "XXX", "XXX", "XXX", "Space", "XXX", "XXX", "XXX", "Right Alt", "Right Windows", "Right Fn", "XXX", "Right Control", "Left Arrow", "Down Arrow", "Right Arrow", "Number Pad 0", "XXX", "Number Pad .", "XXX"}
    };

    bool matrix_valid_ansi[6][22] = {
        /*
         * ESC          F1    F2    F3    F4    F5    F6    F7    F8    F9   F10   F11   F12   XXX   PRN   SCL   PAU      P1    P2    P3    P4 */
        {true, false, true, true, true, true, true, true, true, true, true, true, true, true, false, true, true, true,  true, true, true, true},
	    /* `     1     2     3     4     5     6     7     8     9     0     -     =     BCK   XXX   INS   HOM   PUP     #LCK    #/   #+     #- */
        {true, true,  true, true, true, true, true, true, true, true, true, true, true, true, false, true, true, true,  true, true, true, true},
        /* TAB  XXX    Q     W     E     R     T     Y     U     I     O     P     [     ]     \     DEL   END   PDN      #7     #8   #9     #+ */
        {true, false, true,  true, true, true, true, true, true, true, true, true, true, true, true, true, true, true,  true, true, true, true},
        /* CAP  XXX    A     S     D     F     G     H     J     K     L     ;     '     XXX   ENT   XXX   XXX   XXX      #4     #5   #6   XXX */
        {true, false, true,  true, true, true, true, true, true, true, true, true, true, false, true,false,false,false, true, true, true, false},
	    /* LSHFT XXX   Z     X     C     V     B     N     M     ,     .     /     XXX   RSHFT  XXX   XXX   UP  XXX       #1   #2   #3   #ENTER */
        {true, false, true,  true, true, true, true, true, true, true, true, true, false, true, false,false,true, false,true, true, true, true},
	    /* LCTRL LWIN  LALT  XXX   XXX   XXX  XXX    SPACE XXX   XXX   XXX   RALT  RWIN  FN    RCTRL LEFT  DOWN  RIGHT    #0   XXX   #.   XXX */
        {true, true, true, false,false,false, false, true,false,false,false, true, true, true, true, true, true, true,  true, false,true, false}
    };

    // TODO
    bool matrix_valid_iso[6][22] = {
        /*
         * ESC          F1    F2    F3    F4    F5    F6    F7    F8    F9   F10   F11   F12   XXX   PRN   SCL   PAU      P1    P2    P3    P4 */
        {true, false, true, true, true, true, true, true, true, true, true, true, true, true, false, true, true, true,  true, true, true, true},
	    /* `     1     2     3     4     5     6     7     8     9     0     -     =     BCK   XXX   INS   HOM   PUP     #LCK    #/   #+     #- */
        {true, true,  true, true, true, true, true, true, true, true, true, true, true, true, false, true, true, true,  true, true, true, true},
        /* TAB  XXX    Q     W     E     R     T     Y     U     I     O     P     [     ]     \     DEL   END   PDN      #7     #8   #9     #+ */
        {true, false, true,  true, true, true, true, true, true, true, true, true, true, true, true, true, true, true,  true, true, true, true},
        /* CAP  XXX    A     S     D     F     G     H     J     K     L     ;     '     XXX   ENT   XXX   XXX   XXX      #4     #5   #6   XXX */
        {true, false, true,  true, true, true, true, true, true, true, true, true, true, false, true,false,false,false, true, true, true, false},
	    /* LSHFT XXX   Z     X     C     V     B     N     M     ,     .     /     XXX   RSHFT  XXX   XXX   UP  XXX       #1   #2   #3   #ENTER */
        {true, false, true,  true, true, true, true, true, true, true, true, true, false, true, false,false,true, false,true, true, true, true},
	    /* LCTRL LWIN  LALT  XXX   XXX   XXX  XXX    SPACE XXX   XXX   XXX   RALT  RWIN  FN    RCTRL LEFT  DOWN  RIGHT    #0   XXX   #.   XXX */
        {true, true, true, false,false,false, false, true,false,false,false, true, true, true, true, true, true, true,  true, false,true, false}
    };

    unsigned int matrix_map[CMMK_ROWS_MAX*CMMK_COLS_MAX] = {0xFFFFFFFF,};

    struct cmmk_color_matrix current_matrix;

    std::atomic<bool> dirty;
    std::atomic<bool> force_update;
};

